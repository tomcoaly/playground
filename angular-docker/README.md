# Angular with Docker
## Install Angular
Install Angular and create a new project
```
npm install -g @angular/cli
ng new angular-docker
cd angular-docker
```
## Add Docker support
Create Dockerfile
```
# base image
FROM node:12.2.0

# install chrome for protractor tests
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update && apt-get install -yq google-chrome-stable

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@7.3.9

# add app
COPY . /app

# start app
CMD ng serve --host 0.0.0.0
```
Create .dockerignore
```
node_modules
.git
.gitignore
```
Build the image
```
docker build -t example:dev .
```
Run the image
```
docker run -d -v ${PWD}:/app -v /app/node_modules -p 4201:4200 --name angular-docker --rm example:dev
```
Open in browser: http://localhost:4201
## Tests
Update src/karma.conf.js
```
module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, '../coverage/example'),
      reports: ['html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    // updated
    browsers: ['ChromeHeadless'],
    // new
    customLaunchers: {
      'ChromeHeadless': {
        base: 'Chrome',
        flags: [
          '--no-sandbox',
          '--headless',
          '--disable-gpu',
          '--remote-debugging-port=9222'
        ]
      }
    },
    singleRun: false,
    restartOnFileChange: true
  });
};
```
Update e2e/protractor.conf.js
```
const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './src/**/*.e2e-spec.ts'
  ],
  capabilities: {
    'browserName': 'chrome',
    // new
    'chromeOptions': {
      'args': [
        '--no-sandbox',
        '--headless',
        '--window-size=1024,768'
      ]
    }
  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};
```
Run unit tests
```
docker exec -it angular-docker ng test --watch false
```
Run e2e tests
```
docker exec -it angular-docker ng e2e --port 4202
```
Stop the container
```
docker stop angular-docker
```
## Docker Compose
Create docker-compose.yml
```
version: '3.7'

services:
  angular-docker:
    container_name: angular-docker
    build:
      context: .
      dockerfile: Dockerfile
    volumes:
      - '.:/app'
      - '/app/node_modules'
    ports:
      - '4201:4200'
```
Build the image and start the container:
```
docker-compose up -d --build
```

Open in browser: http://localhost:4201

Run the tests
```
docker-compose exec angular-docker ng test --watch=false
docker-compose exec angular-docker ng e2e --port 4202
```
Stop the container
```
docker-compose stop
```
[Kudos](https://mherman.org/blog/dockerizing-an-angular-app/)